<?php

/**
 * @file
 * Conatins XLSResourceFormat
 */

namespace Drupal\visualn_excel\Plugin\VisualN\RawResourceFormat;

use Drupal\visualn\Core\RawResourceFormatBase;

/**
 * Provides a 'XLS' VisualN raw resource format.
 *
 * @VisualNRawResourceFormat(
 *  id = "visualn_xls",
 *  label = @Translation("XLS"),
 *  output = "local_file_xls",
 * )
 */
class XLSResourceFormat extends RawResourceFormatBase {
  // @todo: maybe provide configuration form
}
