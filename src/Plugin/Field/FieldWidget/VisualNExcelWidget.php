<?php

// @todo: instead of creating an additional widget, the module could alter existing file widget
//   same for other files formats (csv, tsv, maybe json, etc.)
//   though json may be supported by core since doesn't need additional dependencies to process
//
//   maybe add a checkbox with ajax to switch between textfields an selects types

// @todo: check VisualNUrlWidget and VisualNWidget

namespace Drupal\visualn_excel\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\visualn_file_field\Plugin\Field\FieldWidget\VisualNWidget;

use Drupal\file\Entity\File;
use Drupal\Core\Render\Element;

//use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

/**
 * Plugin implementation of the 'visualn_excel' widget.
 *
 * @todo: do no make available as widget (@_FieldWidget) yet, the class is used in visualn_excel.module
 *   to substitute original visualn_file widget class
 *
 * @_FieldWidget(
 *   id = "visualn_excel",
 *   label = @Translation("VisualN Excel"),
 *   field_types = {
 *     "visualn_file"
 *   }
 * )
 */
class VisualNExcelWidget extends VisualNWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // @todo: also the widget should not allow other formats then xls and similar

    // @todo: maybe execute ajax callback at resource format change and allow it to change the configuration form,
    //    specifically data keys subform, though it may be an overhead

    // @todo: maybe allow to select excel sheet though it's not clear how adapter should know about it

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $item = $items[$delta];
    // exclude element with no files uploaded ("add more" button)
    //if (empty($item->fids)) {
    // @todo: this doesn't work for uploaded but not saved files
    //if (empty($item->target_id)) {
    if (empty($item->fids)) {
      return $element;
    }


    // take keys from xls file, convert drawer_fields into selects from textfields
    $element['drawer_container']['#process'][] = [$this, 'processDrawerContainerSubformDrawerFields'];



    // set resource type to xls and disable it so that user could not change it
    // @todo: the class is used as visualn_file widget replacement, so do not set any default value
    //$element['resource_format']['#default_value'] = 'visualn_xls';
    // @todo: do not disable resource_format ?
    //$element['resource_format']['#disabled'] = TRUE;

    // switch data keys textfields to selects at resource format change (trigger ajax on style select)
    $element['resource_format']['#ajax'] = $element['visualn_style_id']['#ajax'];

    return $element;
  }

  public static function processDrawerContainerSubformDrawerFields(array $element, FormStateInterface $form_state, $form) {
    // check resource_format, do nothing if not visualn_xls
    $style_form_parents = array_slice($element['#parents'], 0, -1);
    $resource_format = $form_state->getValue(array_merge($style_form_parents, ['resource_format']));
    if ($resource_format != 'visualn_xls') {
      return $element;
    }

    // @todo: check this line
    $item = $element['#item'];

    // get uploaded file url
    // @todo: check if it is ok to use fids instead of target_id (target_id is empty until file saved)
    $file = File::load($item->fids[0]);
    //$file = File::load($item->target_id);


    $header_keys = static::getHeaderKeys($file);


    // make covert drawer_fields into selects from textfields and add keys from xls header row
    $visualn_style_id = $form_state->getValue(array_merge($style_form_parents, ['visualn_style_id']));
    if ($visualn_style_id) {
      $resource_format = $form_state->getValue(array_merge($style_form_parents, ['resource_format']));
      if ($resource_format == 'visualn_xls') {
        // since drawer_fields are attached in a separate #process callback,
        // attach #header_keys to a the element and process in a new #process callback
        $element[$visualn_style_id]['drawer_fields']['#process'][] = [get_called_class(), 'processDrawerContainerSubformDrawerFields2'];
        $element[$visualn_style_id]['drawer_fields']['#header_keys'] = $header_keys;
      }
    }

    return $element;
  }

  protected static function getHeaderKeys(File $file) {

    // @todo: also possible just to urldecode() the $file_url = $file->url();
    $file_uri = $file->getFileUri();
    $file_full_path = \Drupal::service('file_system')->realpath($file_uri);

    // get relative path of the file for internal usage
    // the name already contains "/" at the beginning
    $relative_filename = '.' . str_replace(DRUPAL_ROOT, '', $file_full_path);

    $reader = new Xls();
    $spreadsheet = $reader->load($relative_filename);


    // @todo: we suppose that there maybe enough columns for all data columns between A and Z
    $header_keys = [];
    foreach (range('A', 'Z') as $letter) {
      $header_key = $spreadsheet->getActiveSheet()->getCell($letter . '1')->getValue();
      $header_keys[] = $header_key;
    }
    $header_keys = array_filter($header_keys);
    $header_keys = array_combine($header_keys, $header_keys);

    return $header_keys;
  }

  public static function processDrawerContainerSubformDrawerFields2(array $element, FormStateInterface $form_state, $form) {

    $header_keys = $element['#header_keys'];
    $style_form_parents = array_slice($element['#parents'], 0, -3);

    $visualn_style_id = $form_state->getValue(array_merge($style_form_parents, ['visualn_style_id']));
    $visualn_data = $form_state->getValue(array_merge($style_form_parents, ['visualn_data']));
    $visualn_data = unserialize($visualn_data);

    // replace textfields with selects
    foreach (Element::children($element) as $key) {
      $default_value = !empty($visualn_data['drawer_fields'][$key]) ? $visualn_data['drawer_fields'][$key] : '';
      $element[$key]['field'] = [
        '#type' => 'select',
        '#options' => $header_keys,
        '#empty_option' => t('- None -'),
        '#default_value' => $default_value,
      ];
    }

    return $element;
  }

}
