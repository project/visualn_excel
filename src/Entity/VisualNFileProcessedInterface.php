<?php

namespace Drupal\visualn_excel\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VisualN File Processed entities.
 *
 * @ingroup visualn_excel
 */
interface VisualNFileProcessedInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the VisualN File Processed processed path.
   *
   * @return string
   *   Unprocessed path of the VisualN File Processed.
   */
  public function getUnprocessed();

  /**
   * Gets the VisualN File Processed unprocessed path.
   *
   * @return string
   *   Unprocessed path of the VisualN File Processed.
   */
  public function setUnprocessed($unprocessed);

  /**
   * Gets the VisualN File Processed processed path.
   *
   * @return string
   *   Processed path of the VisualN File Processed.
   */
  public function getProcessed();

  /**
   * Sets the VisualN File Processed processed path.
   *
   * @param string processed
   *   The VisualN File Processed processed path.
   *
   * @return \Drupal\visualn_excel\Entity\VisualNFileProcessedInterface
   *   The called VisualN File Processed entity.
   */
  public function setProcessed($processed);

  /**
   * Gets the VisualN File Processed creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VisualN File Processed.
   */
  public function getCreatedTime();

  /**
   * Sets the VisualN File Processed creation timestamp.
   *
   * @param int $timestamp
   *   The VisualN File Processed creation timestamp.
   *
   * @return \Drupal\visualn_excel\Entity\VisualNFileProcessedInterface
   *   The called VisualN File Processed entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the VisualN File Processed published status indicator.
   *
   * Unpublished VisualN File Processed are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the VisualN File Processed is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a VisualN File Processed.
   *
   * @param bool $published
   *   TRUE to set this VisualN File Processed to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\visualn_excel\Entity\VisualNFileProcessedInterface
   *   The called VisualN File Processed entity.
   */
  public function setPublished($published);

}
