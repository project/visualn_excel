<?php

namespace Drupal\visualn_excel\Plugin\VisualN\Adapter;

use Drupal\visualn\ResourceInterface;
use Drupal\visualn\Plugin\VisualN\Adapter\RemoteDsvToJSArrayAdapter;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Drupal\visualn\Helpers\VisualN;

/**
 * Provides a 'LocalFileCsvToDataArray' VisualN adapter. Generally this is a wrapper around DSV Adapter.
 *
 * @VisualNAdapter(
 *  id = "visualn_csv_data",
 *  label = @Translation("Local File CSV To Data Array Adapter"),
 *  input = "local_file_csv",
 *  output = "generic_data_array",
 * )
 */
class LocalFileCsvToDataArrayAdapter extends RemoteDsvToJSArrayAdapter {

  // @todo: no need to extend RemoteDsvToJSArrayAdapter adapter here

  /**
   * @inheritdoc
   */
  public function prepareBuild(array &$build, $vuid, ResourceInterface $resource) {

    $file_url = $resource->file_url;
    // @todo: here we have the full realpath of the file
    //    so this should be whether checked or maybe some flag used (or maybe some function
    //    supports both inputs)


    //$inputFileName = './' . file_url_transform_relative($file_url);
    $inputFileName = urldecode($file_url);

    $reader = new Csv();
    $spreadsheet = $reader->load($inputFileName);
    $data = $this->getArrayData($spreadsheet);


    $output_type = 'generic_data_array';
    if (!empty($data)) {
      //$first_row = $data[0];
      $first_row = array_shift($data);

      $new_data = [];
      foreach ($data as $row) {
        // use $first_row as keys for new row
        $new_row = array_combine($first_row, $row);
        $new_data[] = $new_row;
      }
      $data = $new_data;
    }

    $raw_input =  [
      'data' => $data,
    ];

    $resource = VisualN::getResourceByOptions($output_type, $raw_input);

    return $resource;
  }

  // @todo: this method is take completely from xls adapter so review the code

  // @todo: And it' always better to let PHPExcel/PHPSpreadsheet identify the filetype itself rather than explicitly specifying a Reader, because not all spreadsheet files are what they claim to be
  // https://github.com/PHPOffice/PhpSpreadsheet/issues/97
  public function getArrayData($spreadsheet) {
    //$spreadsheet = PhpSpreadsheet\IOFactory::load( 'myfile.wherever' );
    $worksheet = $spreadsheet->getActiveSheet();
    $rows = [];
    foreach ($worksheet->getRowIterator() AS $row) {
      $cellIterator = $row->getCellIterator();
      $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
      $cells = [];
      foreach ($cellIterator as $cell) {
        $cells[] = $cell->getValue();
      }
      $rows[] = $cells;
    }

    return $rows;
  }

}
