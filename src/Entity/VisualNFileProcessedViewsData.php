<?php

namespace Drupal\visualn_excel\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VisualN File Processed entities.
 */
class VisualNFileProcessedViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
