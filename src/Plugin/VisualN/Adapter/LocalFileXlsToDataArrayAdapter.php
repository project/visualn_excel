<?php

namespace Drupal\visualn_excel\Plugin\VisualN\Adapter;

use Drupal\visualn\Core\AdapterBase;
use Drupal\visualn\ResourceInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Drupal\visualn\Helpers\VisualN;

/**
 * Provides a 'LocalFileXlsToDataArray' VisualN adapter. Generally this is a wrapper around DSV Adapter.
 *
 * @VisualNAdapter(
 *  id = "visualn_xls_data",
 *  label = @Translation("Local File XLS To Data Array Adapter"),
 *  input = "local_file_xls",
 *  output = "generic_data_array",
 * )
 */
class LocalFileXlsToDataArrayAdapter extends AdapterBase {

  /**
   * @inheritdoc
   */
  public function prepareBuild(array &$build, $vuid, ResourceInterface $resource) {

    $file_url = $resource->file_url;
    // @todo: get internal file path relative to the the DRUPAL_ROOT


    // @todo: relative file path should be provided by the formatter since internal file used here
    //    what if external file is needed to be processed? should it be downloaded somewhere else?
    // @todo: external files can't be processed like this. The same notice
    //    for widget.
    $inputFileName = './' . file_url_transform_relative($file_url);
    $inputFileName = urldecode($inputFileName);

    $reader = new Xls();
    $spreadsheet = $reader->load($inputFileName);

    //dsm($this->getArrayData($spreadsheet));

    $output_type = 'generic_data_array';
    $data = $this->getArrayData($spreadsheet);

    if (!empty($data)) {
      //$first_row = $data[0];
      $first_row = array_shift($data);

      $new_data = [];
      foreach ($data as $row) {
        // use $first_row as keys for new row
        $new_row = array_combine($first_row, $row);
        $new_data[] = $new_row;
      }
      $data = $new_data;
    }



    $raw_input =  [
      'data' => $data,
    ];

    // @todo: load resource plugin
    $resource = VisualN::getResourceByOptions($output_type, $raw_input);

    //$new_resource->data = $data;

    return $resource;
/*

    // @todo: allow to select xls file sheet



    // Attach drawer config to js settings
    // Also attach settings from the parent method
    parent::prepareBuild($build, $vuid, $resource); // @todo: here updated resource may theoretically be needed
    // @todo: $resource = parent::prepareBuild($build, $vuid, $resource); (?)


    return $resource;
*/
  }

  // @todo: And it' always better to let PHPExcel/PHPSpreadsheet identify the filetype itself rather than explicitly specifying a Reader, because not all spreadsheet files are what they claim to be
  // https://github.com/PHPOffice/PhpSpreadsheet/issues/97
  public function getArrayData($spreadsheet) {
    //$spreadsheet = PhpSpreadsheet\IOFactory::load( 'myfile.wherever' );
    $worksheet = $spreadsheet->getActiveSheet();
    $rows = [];
    foreach ($worksheet->getRowIterator() AS $row) {
      $cellIterator = $row->getCellIterator();
      $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
      $cells = [];
      foreach ($cellIterator as $cell) {
        $cells[] = $cell->getValue();
      }
      $rows[] = $cells;
    }

    return $rows;
  }

}
