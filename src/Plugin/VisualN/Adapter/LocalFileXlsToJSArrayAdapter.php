<?php

namespace Drupal\visualn_excel\Plugin\VisualN\Adapter;

use Drupal\visualn\ResourceInterface;
use Drupal\visualn\Plugin\VisualN\Adapter\RemoteDsvToJSArrayAdapter;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Drupal\visualn_excel\Entity\VisualNFileProcessed;
use Drupal\visualn\Helpers\VisualN;

//use Drupal\visualn\Core\AdapterBase;

// instead of extending the RemoteDsvToJSArrayAdapter it may return remote_generic_csv resource
// and be used as part of adapter subchain (which is generally a better and more correct practice)

// or it may be even replaced by a pair of adapters: LocalFileXlsToDataArrayAdapter and DataArrayToJSArrayAdapter
// this is a good case to show that the same thing may be done multiple ways

// @todo: actually this is a good use case example for multiple adapters in a chain (though chain builder should
//    be modified accordingly

// @todo: maybe convert to LocalFileXlsToRemoteCsv (or even LocalFileXlsToLocalCsv if appropriate)

/**
 * Provides a 'LocalFileXlsToJSArray' VisualN adapter. Generally this is a wrapper around DSV Adapter.
 *
 * The adapter converts local XLS file to local CSV file an relies
 * on RemoteDsvToJSArrayAdapter to further convert resource to 'generic_js_data_array' one.
 *
 * @VisualNAdapter(
 *  id = "visualn_xls",
 *  label = @Translation("Local File XLS To JS Array Adapter"),
 *  input = "local_file_xls",
 * )
 */
class LocalFileXlsToJSArrayAdapter extends RemoteDsvToJSArrayAdapter {

  // @todo: generally this is a DSV (delimiter separated values) file
  // @todo: convert it to general purpose adapter for formatted column text

  // @todo: this can be presented as pair of adapters:
  //   LocalFileXlsToDataArrayAdapter and DataArrayToJSArrayAdapter
  //   basically this should include some code similar to DataArrayToJSArrayAdapter
  //   instead of relying on RemoteDsvToJSArrayAdapter and js-side csv-to-data conversions

  // @todo: adapter configuration should allow to change files storage location
  //   see FileItem::doGetUploadLocation for example

  /**
   * @inheritdoc
   */
  public function prepareBuild(array &$build, $vuid, ResourceInterface $resource) {
    // @todo: check mimetype:  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet

    // This setting is required by the DSV Adapter method
    // @todo: though it should be set in source provder
    //$resource->file_mimetype = 'application/vnd.ms-excel';

    // @todo: convert xls to csv


    // @todo: this should be local file path or uri or contain both along with file_url
    $file_url = $resource->file_url;

    // @todo: get internal file path relative to the the DRUPAL_ROOT


    // @todo: relative file path should be provided by the formatter (i.e. with the resource)
    //    since internal file used here
    //    what if external file is needed to be processed? should it be downloaded somewhere else?
    // @todo: external files can't be processed like this. The same notice
    //    for widget.
    // @todo: what if an alias is used, will it work then?


    // @todo: here relative to the file system root file path expected
    $relative_filename = file_url_transform_relative($file_url);
    $relative_filename = urldecode($relative_filename);

    // @todo: relative file name should be provided by the resource,
    //   the file name itself should be a real internal filesystem path
    //   relative to the DRUPAL_ROOT and without leading slash
    //   otherwise full path is considered
    // @todo: add additional checks then and remove ltrim()

    // @todo: also store file scheme for both original (unprocessed) and processed files
    $relative_filename = ltrim($relative_filename, '/');
    $query = \Drupal::entityQuery('visualn_file_processed')
      ->condition('unprocessed', $relative_filename)
      ->range(0, 1);
    $entity_ids = $query->execute();
    if ($entity_ids) {
      $row_id = reset($entity_ids);
      $file_processed_entity = VisualNFileProcessed::load($row_id);
      $destination_filename_full = $file_processed_entity->get('processed')->value;

      // check if file exists in filesystem
      // @todo: also check other data (maybe file hash, changed time etc.)
      //   when provided by visualn_file_processed entity
      if (!file_exists($destination_filename_full)) {
        $destination_filename_full = NULL;
      }
    }

    // @todo: update existing visualn_file_processed entry when exists
    //   but the processed file was deleted
    if (empty($destination_filename_full)) {
      // @todo: see ImageItem::generateSampleValue() for creating file example
      //  and ImageStyle::createDerivative()

      // @todo: if directory exists but not writable, do nothing - no sense to
      //   convert the file (just show/log an error)
      $destination_dir = 'sites/default/files/visualn/visualn_excel';
      file_prepare_directory($destination_dir, FILE_CREATE_DIRECTORY);
      // @todo: maybe just generate a random unique filename or at least remove original extension
      $destination_filename = str_replace('.', '_', basename($relative_filename)) . '.csv';

      // get full path to the destination file
      $destination_filename_full = file_create_filename($destination_filename, $destination_dir);
      //$destination_filename_full = file_create_filename('result.csv', 'sites/default/files');

      //$destination = $destination_dir . '/' . basename($path);

      $reader = new Xls();
      $spreadsheet = $reader->load('./' . $relative_filename);

      // @todo: allow to select xls file sheet


      // @todo: remove generated file on xls file deletion
      //   (maybe listen to the cooresponding hook and check fid)

      $writer = new Csv($spreadsheet);
      $writer->save('./' . $destination_filename_full);

      // Use visualn_file_processed entity for storing files cross-references
      // and locations and other data / info to not regenerate a new file if one already exists.
      $file_processed_entity = VisualNFileProcessed::create([
        'user_id' => \Drupal::currentUser()->id(),
        'unprocessed' => $relative_filename,
        'processed' => $destination_filename_full,
        // @todo: is langcode required here?
        'langcode' => 'en',
        'status' => 1,
      ]);

      // @todo: check for any errors on saving the file (check if file exists)
      $file_processed_entity->save();
    }
    // @todo: apache seems to still send the file even if deleted, maybe add some unique
    //   parameter (?unique hash) to the file path when sent to client


    // @todo: set correct output_type for the resource
    //   actually this is not completely "remote" since stored on the same server
    //   and may need a simpler logic when talking of caching (local files don't need
    //   to be cached to guarantee file presence when needed), or maybe just add
    //   a special flag to the resource type that would point to this fact and would be
    //   considered by the respective adapter

    // create resultant resource object
    $output_type = 'remote_generic_csv';
    $raw_input = [
      'file_mimetype' => 'text/csv',
      // create a (relative) url from real file path
      // @todo: maybe convert to an absolute url path
      'file_url' => '/' . $destination_filename_full,
    ];
    $resource = VisualN::getResourceByOptions($output_type, $raw_input);


    // Convert 'remote_generic_csv' to 'generic_js_data_array' using parent adapter method
    // Attach drawer config to js settings
    // Also attach settings from the parent method

    // expected resource type here is 'generic_js_data_array'
    $resource = parent::prepareBuild($build, $vuid, $resource);


    return $resource;
  }

}
