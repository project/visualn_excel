<?php

namespace Drupal\visualn_excel;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the VisualN File Processed entity.
 *
 * @see \Drupal\visualn_excel\Entity\VisualNFileProcessed.
 */
class VisualNFileProcessedAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\visualn_excel\Entity\VisualNFileProcessedInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished visualn file processed entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published visualn file processed entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit visualn file processed entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete visualn file processed entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add visualn file processed entities');
  }

}
